/* eslint-disable no-param-reassign */
const URLImportPlugin = require('webpack-external-import/webpack');

module.exports = function({ env }) {
    return {
        ...(!process.env.REACT_APP_CRA_MF_DEV
        ? null
        : {
            devServer: {
                port: '3004',
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods':
                    'GET, POST, PUT, DELETE, PATCH, OPTIONS',
                    'Access-Control-Allow-Headers':
                    'X-Requested-With, content-type, Authorization'
                }
            }
        }),
         webpack: {
            configure: { /* Any webpack configuration options: https://webpack.js.org/configuration */ },
            configure: (webpackConfig, { env, paths }) => {
                webpackConfig.devtool = 'inline-source-map';
                webpackConfig.plugins.push(
                    new URLImportPlugin({
                        manifestName: 'exporter-site',
                        fileName: "importManifest.js",
                        basePath: ``,
                        publicPath: `//localhost:3004/`,
                        transformExtensions: /^(gz|map)$/i,
                        writeToFileEmit: false,
                        seed: null,
                        filter: null,
                        debug: true,
                        map: null,
                        generate: null,
                        sort: null
                    })
                );
                webpackConfig.optimization.runtimeChunk = true;
            
                return webpackConfig; 
            }
        },
    };
};