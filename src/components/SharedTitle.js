import React from 'react';

export const SharedTitle = ({ title }) => {
  window.wasExternalFunctionCalled = true;
  console.log("SharedTitleComponent interleaving successful");
  return (
    <React.Fragment>
      <h1>{title}</h1>
      <div>This is a Shared Title Component</div>
      <br></br>
    </React.Fragment>
  );
};

export const sharedTitleAlert = message => {
  alert(message);
};
